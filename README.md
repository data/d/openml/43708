# OpenML dataset: Grasping-Dataset

https://www.openml.org/d/43708

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
At Shadow Robot, we are leaders in robotic grasping and manipulation. As part of our Smart Grasping System development, we're developing different algorithms using machine learning. 
This first public dataset was created to investigate the use of machine learning to predict the stability of a grasp. Due to the limitations of the current simulation, it is a restricted dataset - only grasping a ball. The dataset is annotated with an objective grasp quality and contains the different data gathered from the joints (position, velocity, effort).
You can find all the explanations for this dataset over on Medium.
Inspiration
I'll be more than happy to discuss this dataset as well as which dataset you'd like to have to try your hands at solving real world robotic problems focused on grasping using machine learning. Let's connect on twitter (ugocupcic)!

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43708) of an [OpenML dataset](https://www.openml.org/d/43708). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43708/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43708/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43708/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

